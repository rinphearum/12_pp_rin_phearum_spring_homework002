package com.example.spring_howework2.controller;

import com.example.spring_howework2.model.entity.Customer;
import com.example.spring_howework2.model.request.CustomerRequest;
import com.example.spring_howework2.model.response.CustomerResponse;
import com.example.spring_howework2.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
@RequestMapping("/api/v1")
@RestController
public class CustomerController {
    ArrayList<Customer> customers = new ArrayList<>();
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/get-all-customer")
    public List<Customer> getAllCustomer(){
        return customerService.getAllCustomer();
    }
    @GetMapping("/get-all-customer/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable Integer id){
        Customer customer = customerService.getCustomerById(id);
        CustomerResponse<Customer> response = new CustomerResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Succesfully",
                customer
        );
       return ResponseEntity.ok(response);
    }
    @PostMapping("/add-new-customer")
    public ResponseEntity<?> insertCustomer(@RequestBody Customer customer){
        return ResponseEntity.ok(new CustomerResponse<Customer>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Insert customer successfully!",
                customerService.insertCustomer(customer)
        ));
    }
    @PutMapping("/update-customer/{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable Integer id,@RequestBody CustomerRequest customerRequest){
        customerService.updateById(id,customerRequest);
        return ResponseEntity.ok(new CustomerResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Updated successfully!",
                null
        ));
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Integer id){
        customerService.deleteCustomerById(id);
        return ResponseEntity.ok(new CustomerResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "delete Successfully",
                null
        ));
    }
}
