package com.example.spring_howework2.repository;

import com.example.spring_howework2.model.entity.Customer;
import com.example.spring_howework2.model.request.CustomerRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Select("""
            SELECT * FROM customers
            """)
    @Result(property = "cusId",column = "customer_id")
    @Result(property = "cusName",column = "customer_name")
    @Result(property = "cusAddress",column = "customer_address")
    @Result(property = "cusPhone",column = "customer_phone")
    List<Customer> getAllCustomer();
    @Select("""
            SELECT * FROM customers WHERE customer_id = #{id}
            """)
    @Result(property = "cusId",column = "customer_id")
    @Result(property = "cusName",column = "customer_name")
    @Result(property = "cusAddress",column = "customer_address")
    @Result(property = "cusPhone",column = "customer_phone")
    Customer getCustomerById(Integer id);
    @Select("""
            INSERT INTO customers (customer_name,customer_address,customer_phone)   VALUES(#{customer.cusName},
            #{customer.cusAddress},#{customer.cusPhone})
            RETURNING *
            """)
    @Result(property = "cusId",column = "customer_id")
    @Result(property = "cusName",column = "customer_name")
    @Result(property = "cusAddress",column = "customer_address")
    @Result(property = "cusPhone",column = "customer_phone")
    Customer insetCustomer(@Param("customer") Customer customer);
    @Update("""
            UPDATE customers
            SET customer_name = #{customer.cusName},customer_address= #{customer.cusAddress},customer_phone=#{customer.cusPhone}
            WHERE customer_id = #{id}
            """)
    @Result(property = "cusId",column = "customer_id")
    @Result(property = "cusName",column = "customer_name")
    @Result(property = "cusAddress",column = "customer_address")
    @Result(property = "cusPhone",column = "customer_phone")
    void updateById(Integer id,@Param("customer") CustomerRequest customerRequest);
    @Delete("""
            DELETE FROM customers 
            WHERE customer_id = #{id}
            """)
    void deleteCustomerById(Integer id);
}
