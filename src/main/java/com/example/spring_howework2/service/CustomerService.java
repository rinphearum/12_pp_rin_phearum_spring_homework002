package com.example.spring_howework2.service;

import com.example.spring_howework2.model.entity.Customer;
import com.example.spring_howework2.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomer();

    Customer getCustomerById(Integer id);

    Customer insertCustomer(Customer customer);

    void updateById(Integer id, CustomerRequest customerRequest);

    void deleteCustomerById(Integer id);
}
