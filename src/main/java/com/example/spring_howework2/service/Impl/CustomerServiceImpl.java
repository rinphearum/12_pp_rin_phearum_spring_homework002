package com.example.spring_howework2.service.Impl;


import com.example.spring_howework2.model.entity.Customer;
import com.example.spring_howework2.model.request.CustomerRequest;
import com.example.spring_howework2.repository.CustomerRepository;
import com.example.spring_howework2.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public Customer insertCustomer(Customer customer) {
        return customerRepository.insetCustomer(customer);
    }

    @Override
    public void updateById(Integer id, CustomerRequest customerRequest) {
         customerRepository.updateById(id,customerRequest);
    }

    @Override
    public void deleteCustomerById(Integer id) {
        customerRepository.deleteCustomerById(id);
    }
}
