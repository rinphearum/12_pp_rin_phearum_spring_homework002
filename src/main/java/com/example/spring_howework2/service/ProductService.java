package com.example.spring_howework2.service;

import com.example.spring_howework2.model.entity.Customer;
import com.example.spring_howework2.model.entity.Product;
import com.example.spring_howework2.model.request.CustomerRequest;
import com.example.spring_howework2.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Customer> getAllProduct();

    Customer getCProductById(Integer id);

    Customer insertProduct(Customer customer);

    void updateById(Integer id, ProductRequest productRequest);

    void deleteProductById(Integer id);

    void insertProduct(Product product);

    Customer getProdcutById(Integer id);
}
