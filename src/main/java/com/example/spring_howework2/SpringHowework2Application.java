package com.example.spring_howework2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringHowework2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringHowework2Application.class, args);
    }

}
