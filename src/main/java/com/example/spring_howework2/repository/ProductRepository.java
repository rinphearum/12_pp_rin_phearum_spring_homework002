package com.example.spring_howework2.repository;

import com.example.spring_howework2.model.entity.Customer;
import com.example.spring_howework2.model.entity.Product;
import com.example.spring_howework2.model.request.CustomerRequest;
import com.example.spring_howework2.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {
    @Select("""
            SELECT * FROM customers
            """)
    @Result(property = "proId",column = "product_id")
    @Result(property = "proName",column = "product_name")
    @Result(property = "proPrice",column = "product_price")
    List<Customer> getAllProduct();
    @Select("""
            SELECT * FROM products WHERE product_id = #{id}
            """)
    @Result(property = "proId",column = "product_id")
    @Result(property = "proName",column = "product_name")
    @Result(property = "proPrice",column = "product_price")
    Customer getCustomerById(Integer id);
    @Select("""
            INSERT INTO products (product_name,product_price)   VALUES(#{product.proName},
            #{product.proPrice})
            RETURNING *
            """)
    @Result(property = "proId",column = "product_id")
    @Result(property = "proName",column = "product_name")
    @Result(property = "proPrice",column = "product_price")
    Customer insetProduct(@Param("product") Product product);
    @Update("""
            UPDATE products
            SET product_name = #{product.proName},product_price= #{product.proPrice}
            WHERE product_id = #{id}
            """)
    @Result(property = "proId",column = "product_id")
    @Result(property = "proName",column = "product_name")
    @Result(property = "proPrice",column = "product_price")
    void updateById(Integer id,@Param("product") ProductRequest productRequest);
    @Delete("""
            DELETE FROM products 
            WHERE product_id = #{id}
            """)
    void deleteProductById(Integer id);
}
