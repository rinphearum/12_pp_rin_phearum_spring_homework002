package com.example.spring_howework2.controller;

import com.example.spring_howework2.model.entity.Customer;
import com.example.spring_howework2.model.entity.Product;
import com.example.spring_howework2.model.request.ProductRequest;
import com.example.spring_howework2.model.response.CustomerResponse;
import com.example.spring_howework2.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
@RequestMapping("/api/v1")
@RestController
public class ProductController {
    ArrayList<Customer> customers = new ArrayList<>();
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    @GetMapping("/get-all-customer")
    public List<Customer> getAllCustomer(){
        return productService.getAllProduct();
    }
    @GetMapping("/get-all-customer/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable Integer id){
        Customer customer = productService.getProdcutById(id);
        CustomerResponse<Customer> response = new CustomerResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Succesfully",
                customer
        );
        return ResponseEntity.ok(response);
    }
    @PostMapping("/add-new-customer")
    public ResponseEntity<?> insertProduct(@RequestBody Product product){
        return ResponseEntity.ok(new CustomerResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Insert customer successfully!",
                productService.insertProduct(product)
        ));
    }
    @PutMapping("/update-customer/{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable Integer id,@RequestBody ProductRequest productRequest){
        productService.updateById(id,productRequest);
        return ResponseEntity.ok(new CustomerResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Updated successfully!",
                null
        ));
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Integer id){
        productService.deleteProductById(id);
        return ResponseEntity.ok(new CustomerResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "delete Successfully",
                null
        ));
    }
}
