package com.example.spring_howework2.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerResponse<T> {
    private LocalDateTime time;
    private HttpStatus status;
    private String message;
    private T  payload;
}
