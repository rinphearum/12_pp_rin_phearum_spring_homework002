package com.example.spring_howework2.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRequest {
    private String cusName;
    private String cusAddress;
    private String cusPhone;
}
